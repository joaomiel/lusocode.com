<?php include 'header.php'; ?>

        <!--========== SWIPER SLIDER ==========-->
        <div class="s-swiper js__swiper-slider">
            <!-- Swiper Wrapper -->
            <div class="swiper-wrapper">
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/13.jpg');">
                    <div class="container g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="g-margin-b-50--xs">
                                    <h1 class="g-font-size-32--xs g-font-size-45--sm g-font-size-60--md g-color--white">Tenha um Website <br>para o seu Negócio</h1>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">Ajude novos clientes a encontrar o seu negócio.</p>
                                </div>
                         <!--       <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/14.jpg');">
                    <div class="container g-text-right--xs g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="g-margin-b-50--xs">
                                    <h2 class="g-font-size-32--xs g-font-size-45--sm g-font-size-55--md g-color--white">Divulgue <br> seu negócio</h2>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">Aumento a receita do seu Negócio, com as nossas soluções de publicidade digital.</p>
                                </div>
                               <!-- <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Swiper Wrapper -->

            <!-- Pagination -->
            <div class="s-swiper__pagination-v1 s-swiper__pagination-v1--bc s-swiper__pagination-v1--white js__swiper-pagination"></div>
        </div>
        <!--========== END SWIPER SLIDER ==========-->

        <!--========== PAGE CONTENT ==========-->
        <!-- Services -->
        <div  id="sobre-nos" class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="row g-margin-b-10--xs">
                <div class="col-md-6 g-margin-b-60--xs g-margin-b-0--lg">
                    <!-- Masonry Grid -->
                    <div class="row g-row-col--5 g-overflow--hidden js__masonry">
                        <div class="col-xs-6 js__masonry-sizer"></div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs g-margin-b-0--sm js__masonry-item">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="img-responsive" src="img/400x550/03.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".3s">
                                <img class="img-responsive" src="img/970x647/10.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".5s">
                                <img class="img-responsive" src="img/970x647/11.jpg" alt="Image">
                            </div>
                        </div>
                    </div>
                    <!-- End Masonry Grid -->
                </div>
                <div class="col-md-5 g-margin-b-10--xs g-margin-b-0--lg g-margin-t-10--lg g-margin-l-20--lg">
                    <div class="g-margin-b-30--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-15--xs">Serviços</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--sm">Somos Especialistas em</h2>
                        <p>With more than 40 years of experience in healthcare consulting, we deliver results to help grow your practice. Our comprehensive medical billing services allow you to do what you do best—run your practice.</p>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled col-xs-6 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Criar Website</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Desenvolvimento Apps</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Marketing Digital</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Criação Lojas Online</li>
                        </ul>
                        
                        <ul class="list-unstyled col-xs-6 g-full-width--xs g-ul-li-tb-5--xs">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Design</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Gestão de Redes Sociais</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Publicidade</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>SEO</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Services -->

        <!-- Process -->
        <div id="processo" class="g-bg-color--primary-ltr">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-text-center--xs g-margin-b-100--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Process</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--sm g-color--white">How it Works</h2>
                </div>
                <ul class="list-inline row g-margin-b-100--xs">
                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Make an Appointment</h3>
                                <p class="g-color--white-opacity">Clinics can be privately operated or publicly managed.</p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Primary Diagnostics</h3>
                                <p class="g-color--white-opacity">Clinics can be privately operated or publicly managed.</p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Daily Course</h3>
                                <p class="g-color--white-opacity">Clinics can be privately operated or publicly managed.</p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Be Healthy</h3>
                                <p class="g-color--white-opacity">Clinics can be privately operated or publicly managed.</p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->
                </ul>

                <div class="g-text-center--xs">
                    <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                        <a href="#js__scroll-to-appointment" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50">Make an Appointment</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Process -->



 

           
  <!-- Feedback Form -->
        <div id="contactos" id="js__scroll-to-appointment" class="g-bg-color--sky-light">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-text-center--xs g-margin-b-80--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Feedback</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--md">Send us a note</h2>
                </div>
                <form>
                    <div class="row g-margin-b-40--xs">
                        <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                            <div class="g-margin-b-20--xs">
                                <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Name">
                            </div>
                            <div class="g-margin-b-20--xs">
                                <input type="email" class="form-control s-form-v2__input g-radius--50" placeholder="* Email">
                            </div>
                            <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Phone">
                        </div>
                        <div class="col-sm-6">
                            <textarea class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8" placeholder="* Your message"></textarea>
                        </div>
                    </div>
                    <div class="g-text-center--xs">
                        <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Feedback Form -->

        <!-- Google Map -->
        <section class="s-google-map">
            <div id="js__google-container" class="s-google-container g-height-400--xs"></div>
        </section>
        <!-- End Google Map -->
        <!-- End Form -->
        <!--========== END PAGE CONTENT ==========-->

        <!--========== FOOTER ==========-->
        <footer class="g-bg-color--dark">
            <!-- Links -->

            <!-- End Links -->

            <!-- Copyright -->
            <div class="container g-padding-y-50--xs">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="index.html">
                            <img class="g-width-100--xs g-height-auto--xs" src="img/logo.png" alt="Megakit Logo">
                        </a>
                    </div>
                    <div class="col-xs-6 g-text-right--xs">
                        <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light">Copyright © 2017-2018 LusoCode - Todos os direitos Reservados </p>
                    </div>
                </div>
            </div>
            <!-- End Copyright -->
        </footer>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
        <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/jquery.masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.equal-height.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr2cgi0_ZNfvgoZFUjyyVqVJORDHaDgqc"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="js/global.min.js"></script>
        <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
        <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
        <script type="text/javascript" src="js/components/swiper.min.js"></script>
        <script type="text/javascript" src="js/components/masonry.min.js"></script>
        <script type="text/javascript" src="js/components/equal-height.min.js"></script>
        <script type="text/javascript" src="js/components/parallax.min.js"></script>
        <script type="text/javascript" src="js/components/wow.min.js"></script>
        <script type="text/javascript" src="js/components/google-map.js"></script>
        <script type="text/javascript" src="js/components/google-map2.js"></script>
        
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
